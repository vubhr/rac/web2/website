<?php
class Tablica{
    private $polje = [];
    private $htmlTable = '';
    //private $indeksi = [];
    private $keyHeader = '';
    private $keyBody = '';
    private $klasa = '';
    function __construct($polje, $klasa) {
        $this->polje = $polje;
        $this->klasa = $klasa;
    }
    public function getHtmlTable(){
       $this->htmlTable .= $this->tableHeader($this->polje[0]);
       $this->htmlTable .= $this->tableBody($this->polje);      
       return $this->htmlTable;
    }

    private function tableHeader($zaglavlje){
        $this->keyHeader = '<table class="' . $this->klasa . '"><thead><tr>';
        foreach($zaglavlje as $value){
          $this->keyHeader .= '<th scope="col">' . $value . '</th>';
        }
        return $this->keyHeader .= '</tr></thead>'; 
    }

    private function tableBody($tijelo){
        $this->keyBody = '<tbody><tr>';
        foreach($tijelo as $key => $value){
            if ($key != 0){
               foreach($value as $redak){
                  $this->keyBody .= '<td>' . $redak . '</td>';
               }
               $this->keyBody .= '</tr><tr>';
            }
            
        }
        return $this->keyBody .= '</tr></tbody></table>'; 
    }

}
?>
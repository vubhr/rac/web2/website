<?php
class Menu implements Konstante{
   private $path;

   function __construct($path) {
    $this->path = $path;
   }

  public function getMenu(){
      $code = '<nav class="navbar navbar-inverse navbar-static-top marginBottom-0" role="navigation">  
               <div class="collapse navbar-collapse" id="navbar-collapse-1">
               <ul class="nav navbar-nav">';
      $elementi = array_slice(scandir($this->path), 2);
        foreach ($elementi as $element) {
            if (is_file($element)){
                $code .='<li ><a href="'. $element . '">' . $element . '</a></li>';
            }else{
                if (is_dir($element)){ 
                    $code .=  '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$element.' <b class="caret"></b></a>
                                <ul class="dropdown-menu">';
                    $code .= $this->genSubMenu($this->path . '/' . $element, $element);
                    $code .= '</ul></li>';
                }
            }
        }  
      return $code . '</div></nav>';
  }

  private function genSubMenu($putanja, $zadnji){
    $output = '';
    $elementi = array_slice(scandir($putanja), 2);
    foreach ($elementi as $element) {
        if (is_file($zadnji .'/'.$element)){
            $output .='<li ><a href="'. '/'. Konstante::korijen . '/'.$zadnji . '/' . $element .'">' . $element . '</a></li>';
        }else{
            $output .= '<li class="dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$element.'</a>
                        <ul class="dropdown-menu">';
            $output .= $this->genSubMenu($putanja . '/' . $element, $zadnji. '/'.$element);
            $output .= '</ul></li>';
        }
    }
    return $output;
  }
} 
?>

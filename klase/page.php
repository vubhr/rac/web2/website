<?php
class Page {
    private $title;
    private $page;

    function __construct($title) {
        $this->title = $title;
    }

    public function displayPage($html_body) {
        $this->page ='
        <html lang="hr">
        <head>
           <meta charset="utf-8">
	       <meta name="viewport" content="width=device-width, initial-scale=1">
           <title>'. $this->title . '</title>
           <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
           <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
           <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
           <script>
           $(document).ready(function(){
            $("ul.dropdown-menu [data-toggle=dropdown]").on("click", function(event) {
              event.preventDefault(); 
              event.stopPropagation(); 
              $(this).parent().siblings().removeClass("open");
              $(this).parent().toggleClass("open");
            });
          });
           
           </script>
        <style>
        .banner {
            overflow: hidden;
            background-color: #777;
            font-size: 40px;
          }
          body {
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 17px;
          }
          
          .marginBottom-0 {margin-bottom:0;}

.dropdown-submenu{position:relative;}
.dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
.dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
.dropdown-submenu:hover>a:after{border-left-color:#555;}
.dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}

        </style>
        </head>
        <body>
        <div class="banner">Banner</div>
        <div class="container">
        '. $html_body.'
        </div>
        </body>
        </html>
        ';
        echo $this->page;
    }
}
?>
<?php
class Datoteke{
    private $putanja;
    public $polje;

    function __construct($putanja) {
        $this->putanja = $putanja;
    }
    
    
    //dohvaćam podatke iz csv datoteke
    public function getCSVArray($key){
       $output = [];
       $buffer = [];
       $row = 1;
       if (($handle = fopen($this->putanja, "r")) !== FALSE) {
          while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
              if ($row == 1 && $key){
                  $ključevi = $data;
                  $output[] = $data;
              }else{
                   $n = 0;
                foreach ($data as $podatak){
                    $buffer[$ključevi[$n]] = $podatak;
                    $n++;
                }  
                $output[] = $buffer;
              }
              
              $row++;
          }
          fclose($handle);
       }
       $this->polje = $output;
       return $this;
    }

    public function getText(){
        return 'Ništa ""za sada';
    }
}
?>